package com.manu.prtaskproject.extensions

import java.text.SimpleDateFormat

fun String.dateToTime(): String {
    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val formatter = SimpleDateFormat("dd MMM yyyy")
    val formattedDate = formatter.format(parser.parse(this))
    return formattedDate
}