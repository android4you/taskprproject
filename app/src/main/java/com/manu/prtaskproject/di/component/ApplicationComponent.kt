package com.manu.prtaskproject.di.component

import android.content.Context
import com.manu.prtaskproject.di.modules.NetworkModule
import com.manu.prtaskproject.di.modules.RepositoryModule
import com.manu.prtaskproject.di.modules.ViewModelModule
import com.manu.prtaskproject.ui.fragments.PullRequestApiFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ViewModelModule::class, RepositoryModule::class, NetworkModule::class])
interface ApplicationComponent {


    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): ApplicationComponent
    }
    fun inject(fragment: PullRequestApiFragment)
   // fun inject(mainActivity: MainActivity)
}
