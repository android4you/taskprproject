package com.manu.prtaskproject.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.manu.prtaskproject.ui.fragments.PullRequestsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    /**
     * Binds ViewModelFactory to instantiate ViewModels
     */
    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PullRequestsViewModel::class)
    internal abstract fun bindApiViewModel(viewModel: PullRequestsViewModel): ViewModel




}
