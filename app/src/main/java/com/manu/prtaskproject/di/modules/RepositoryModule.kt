package com.manu.prtaskproject.di.modules


import com.manu.prtaskproject.data.repository.PullRequestRepository
import com.manu.prtaskproject.data.webapi.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideApiRepository(
        apiService: ApiService

    ): PullRequestRepository {
        return PullRequestRepository(apiService)
    }
}
