package com.manu.prtaskproject

import android.app.Application
import com.manu.prtaskproject.di.component.ApplicationComponent
import com.manu.prtaskproject.di.component.DaggerApplicationComponent

class MyApp: Application() {

    val  applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory().create(this.applicationContext)
    }

}
