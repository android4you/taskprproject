package com.manu.prtaskproject.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.manu.prtaskproject.MyApp
import com.manu.prtaskproject.R
import com.manu.prtaskproject.data.model.PullRequest
import com.manu.prtaskproject.data.model.Resource
import com.manu.prtaskproject.di.modules.ViewModelFactory
import com.manu.prtaskproject.extensions.makeGone
import com.manu.prtaskproject.extensions.makeVisible
import com.manu.prtaskproject.ui.adapter.PullRequestAdapter

import kotlinx.android.synthetic.main.fragment_pull_request.*
import javax.inject.Inject

class PullRequestApiFragment: Fragment(R.layout.fragment_pull_request) {
    lateinit var viewModel: PullRequestsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val component = (requireActivity().application as MyApp).applicationComponent
        component.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[PullRequestsViewModel::class.java]
        arguments?.getString("state")?.let {
            fetchPrList(it)
        }
    }

    private fun fetchPrList(state: String){
     viewModel.getAllPullRequests(state).observe(viewLifecycleOwner, Observer { resource ->
            if(resource is Resource.Loading) {
                progressBar.makeVisible()
            }else{
                progressBar.makeGone()
            }

            when(resource){
                is Resource.Error -> {
                    showErrorMessage(resource.exception.message)
                }
                is Resource.Success -> {
                    showPrList(resource.data)
                }
                else ->{}
            }
        })
    }

    private fun showErrorMessage(msg: String?){
        errorTV.text = msg ?: getString(R.string.something_went_wrong)
        recyclerView.makeGone()
        errorTV.makeVisible()
    }

    private fun showPrList(list: List<PullRequest>?){
        list?.let {
            recyclerView.makeVisible()
            errorTV.makeGone()
            val adapter = PullRequestAdapter(requireActivity(),it )
            recyclerView.adapter = adapter
        }

    }
}