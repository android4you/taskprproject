package com.manu.prtaskproject.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.manu.prtaskproject.R
import com.manu.prtaskproject.data.model.PullRequest
import com.manu.prtaskproject.extensions.dateToTime
import com.manu.prtaskproject.extensions.makeVisible

class PullRequestAdapter(private val context: Context, private val dataList: List<PullRequest>) :
        RecyclerView.Adapter<PullRequestAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_repo_details, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val pullRequest = dataList[position]
        pullRequest.apply {
            holder.userNameTV.text = this.user.login
            val createdAtStr = context.getString(R.string.created_at) + this.created_at.dateToTime()
            holder.createdDateTV.text = createdAtStr
            this.closed_at?.let {
                val closedAtStr = context.getString(R.string.closed_at) + it.dateToTime()
                holder.closedDateTV.text = closedAtStr
                holder.closedDateTV.makeVisible()
            }
            holder.titleTV.text = this.title
            Glide.with(context)
                    .load(this.user.avatar_url)
                    .centerCrop()
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.userImageView)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userNameTV: TextView = itemView.findViewById(R.id.nameTV)
        val userImageView: ImageView = itemView.findViewById(R.id.imageView)
        val createdDateTV: TextView = itemView.findViewById(R.id.createdDateTV)
        val closedDateTV: TextView = itemView.findViewById(R.id.closedDateTV)
        val titleTV: TextView = itemView.findViewById(R.id.titleTV)

    }

}