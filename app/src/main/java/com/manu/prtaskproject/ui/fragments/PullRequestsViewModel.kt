package com.manu.prtaskproject.ui.fragments


import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.manu.prtaskproject.data.model.Resource
import com.manu.prtaskproject.data.repository.PullRequestRepository
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class PullRequestsViewModel @Inject constructor(
   private val pullRequestRespository: PullRequestRepository
) : ViewModel() {

    fun getAllPullRequests(state: String) =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Resource.Loading())
            emit(pullRequestRespository.getPullRequests(state))
        }
}