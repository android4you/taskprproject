package com.manu.prtaskproject.data.model


data class PullRequest(
        val id: Int,
        val state: String,
        val title: String,
        val user: User,
        val created_at: String,
        val closed_at: String?,

) {
    data class User(
            val login: String,
            val id: Int,
            val avatar_url: String,

    )


}