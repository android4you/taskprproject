package com.manu.prtaskproject.data.webapi

import com.manu.prtaskproject.data.model.PullRequest
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("octocat/hello-world/pulls?")
    suspend fun getPullRequests(@Query("state") state: String): Response<List<PullRequest>>


}

