package com.manu.prtaskproject.data.repository

import com.manu.prtaskproject.data.model.PullRequest
import com.manu.prtaskproject.data.model.Resource
import com.manu.prtaskproject.data.webapi.ApiService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PullRequestRepository @Inject constructor( private val apiService: ApiService) {

    suspend fun getPullRequests(state: String): Resource<List<PullRequest>> {
        return try {
            val response = apiService.getPullRequests(state)
            if (response.isSuccessful) {
                Resource.Success(response.body())
            }else {
                Resource.Error(Exception(response.message()))

            }
        }catch (ex: Exception){
            return Resource.Error(ex)
        }
    }


}
